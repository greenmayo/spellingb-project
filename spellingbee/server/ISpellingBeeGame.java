package spellingbee.server;

public interface ISpellingBeeGame {
    int getPointsForWord(String attempt);
//    This message should return the number of points that a given word is worth according to the Spelling Bee rules.
    String getMessage(String attempt);
/*This method should check if the word attempt is a valid word or not according to the Spelling Bee rules.
* It should return a message based on the reason it is rejected or a positive message (e.g. ) if it is a valid word.*/
    String getAllLetters();
//    This method should return the set of 7 letters (as a String) that the spelling bee object is storing
    char getCenterLetter();
//    This method should return the set of 7 letters (as a String) that the spelling bee object is storing
    int getScore();
//    This method should return the current score of the user.
    int[] getBrackets();
//    This method will be used in the gui to determine the various point categories.
}
