package spellingbee.server;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Class of spellingBeeGame that implements the game from NYTimes
 * https://www.nytimes.com/puzzles/spelling-bee
 */
public class SpellingBeeGame implements ISpellingBeeGame{
    private final String sevenLetters;
    private final int SET_SIZE = 7;
    private final int centerLetter; // range 0-6 -- should it be String or int though?
    private int currentScore;
    private final String kirillPath = "txtFiles";
    private Collection<String> foundWords = new HashSet<>();
    private static Collection<String> possibleWords = new HashSet<>();
    private static Collection<String> englishWords = new HashSet<>();
    
    public static void main(String[] args) {
    	System.out.println(englishWords.size());
    }

    public HashSet<String> getCheatSheet() { // method used for testing purposes
        return (HashSet<String>) cheatSheet;
    }

    private Collection<String> cheatSheet = new HashSet<>();
    private int[] gameBrackets;
    Random dice = new Random();

    /**
     * Constructor that uses a random string from the letterCombinations.txt and sets the center letter
     * along with a hashSet for this particular 7-letter combination
     * Basically setting up the environment for the game.
     * @throws IOException
     */
    public SpellingBeeGame() { // choosing random set of letters
        List<String> randomLettersList = null;
        try {
            Path pathToLetters = Paths.get(kirillPath + "\\letterCombinations.txt");
            randomLettersList = Files.readAllLines(pathToLetters);
            englishWords = createWordsFromFile(kirillPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.sevenLetters = randomLettersList.get(dice.nextInt(randomLettersList.size()));
        this.centerLetter = dice.nextInt(SET_SIZE);
        createPossibleWords();
        this.gameBrackets = getBrackets();
        this.currentScore = 0;

    }

    /**
     * Hardcoded value constructor
     * @param letters 7 letters to form words
     * @throws IOException
     */
    public SpellingBeeGame(String letters) throws IOException { // manual set of letters (for SimpleGame I suppose?)
        this.sevenLetters = letters;
        this.centerLetter = dice.nextInt(SET_SIZE);
        englishWords = createWordsFromFile(kirillPath);
        createPossibleWords();
        this.gameBrackets = getBrackets();
        this.currentScore = 0;
    }

    /**
     * Inserts all dictionary words into a hashSet
     * @param path path to file
     * @return HashSet of Strings
     * @throws IOException
     */
    public HashSet<String> createWordsFromFile(String path) throws IOException {
        // supposedly path will be something like C:\Kirill\Dawson\Java\spellingb-project\txtFiles
        Path english = Paths.get(path + "\\english.txt"); // will it work?
        return new HashSet<>(Files.readAllLines(english));
    }

    /**
     * Purpose of this method is to filter usable words and put them into a set
     * to use later and shorten the time of lookup at getMessage()
     * word.indexOf(...) gives opportunity to omit center letter check later on
     */
    private void createPossibleWords(){ // fills the possibleWords with content
        for (String word:
             englishWords) {
            if ((word.charAt(0) >= 97) && //only small letters - no personal names
                (word.length() > 3) && // only words with length 4 or more
                word.indexOf(this.sevenLetters.charAt(this.centerLetter))>-1) { // all words with center letter in them
                possibleWords.add(word);
            }
        }
    }

    /**
     * Calculates the points given based on the attempt length and quality(bonus)
     * Since we don't give points for words shorter than 4, loop starts at value 3
     * meaning that for word.length == 4 it iterates once and gives +1 point
     * longer the word, more loop iterations and count++
     * @param attempt user's string
     * @return points obtained
     */
    @Override
    public int getPointsForWord(String attempt) {
        int count = 0;
        for (int i=3; i<attempt.length(); i++){
            count++;
        }
        if (count>3) // meaning that word is more than (4+2) long
            count += checkForBonus(attempt);
        return count;
    }

    /**
     * Returns the bonus points (7) if every character in sevenLetters is encountered in attempt
     * @param attempt user's string
     * @return bonus or no bonus
     */
    private int checkForBonus(String attempt){
        for (int i = 0; i<sevenLetters.length(); i++){
            if (attempt.indexOf(sevenLetters.charAt(i)) == -1) return 0;
        }
        return 7;
    }

    /**
     * Method checks if the word is in the set, if it is then calls getPoints and adds it to
     * the existing words set.
     * If the word is in possibleWords and in foundWords, displays a message
     * If not in possibleWords, displays a message
     * @param attempt user's guess
     * @return attempt message
     */
    @Override
    public String getMessage(String attempt) { // here we check if its a valid word
        String evaluationMessage;
        if (possibleWords.contains(attempt) && !foundWords.contains(attempt)){
            evaluationMessage = "Got it!:" + getPointsForWord(attempt);
            this.currentScore += getPointsForWord(attempt);
            foundWords.add(attempt);
        }
        else if (foundWords.contains(attempt)) evaluationMessage = "Already guessed:0";
        else evaluationMessage = "Missed:0";

        return evaluationMessage;
    }

    @Override
    public String getAllLetters() {
        return this.sevenLetters;
    }

    @Override
    public char getCenterLetter() {
        return this.sevenLetters.charAt(this.centerLetter);
    }

    @Override
    public int getScore() {
        return this.currentScore;
    }

    /**
     * Checks all the possible words for letters in sevenLetters
     * then calculates the score spread using 25% value, the 50% value, the 75%, the 90%, and the 100%.
     * @return array of values for the scoreboard
     */
    @Override
    public int[] getBrackets() {
        int maxPoints = 0;
        for (String word:
             possibleWords) {
            if (checkSingleWord(word)) // adds word to set if is a good one
            {
                cheatSheet.add(word);
                maxPoints += getPointsForWord(word);
            }
        }
        return new int[]{
                (int)(maxPoints*0.25),
                (int)(maxPoints*0.50),
                (int)(maxPoints*0.75),
                (int)(maxPoints*0.90),
                maxPoints
        };
    }

    /**
     * Helper method for getBrackets that checks if the word can be made of 7 letters
     * @param word is being checked
     * @return false if the word has at least one letter not in sevenLetters
     */
    private boolean checkSingleWord(String word){
        for (int i = 0; i < word.length(); i++) {
            if (this.sevenLetters.indexOf(word.charAt(i)) ==-1)
                return false;
        }
        return true;
    }

    /**
     * I am choosing to add this method and to create a separate field for the brackets array
     * to make less changes right now and to keep everything sort of modular
     * Format for brackets 1:2:3:4:5, first element being the lowest
     * @return String of a specific format when a client(ScoreTab) calls for brackets
     */
    public String getGameBrackets() {
        String bracketsString = this.gameBrackets[0]+ ":" +
                this.gameBrackets[1]+ ":" +
                this.gameBrackets[2]+ ":" +
                this.gameBrackets[3]+ ":" +
                this.gameBrackets[4];
        return bracketsString;
    }
}
