package spellingbee.network;

import java.io.IOException;

import spellingbee.server.*;

/**
 * This class will run on the server side and is used to connect the server code to the backend business code.
 * This class is where the "protocol" will be defined.
 */
public class ServerController{
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.

	private SpellingBeeGame spellingBee = new SpellingBeeGame();

	/**
	 * Action is the method where the protocol translation takes place.
	 * This method is called every single time that the client sends a request to the server.
	 * It takes as input a String which is the request coming from the client. 
	 * It then does some actions on the server (using the ISpellingBeeGame object)
	 * and returns a String representing the message to send to the client
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 */
	public String action(String inputLine) {
		if (inputLine.startsWith("getBrackets")) {
			return spellingBee.getGameBrackets();
		}
		else if (inputLine.equals("getAllLetters")) {
		 	return spellingBee.getAllLetters();
		 }
		else if(inputLine.equals("getCenterLetter")) {
			return ""+spellingBee.getCenterLetter();
		}
		else if(inputLine.equals("getScore")){
			return ""+spellingBee.getScore();
		}
		else if (inputLine.startsWith("play")){
			String s =null;
			try {
			String[] text = inputLine.split(":");
			s = spellingBee.getMessage(text[1]);
			} catch(ArrayIndexOutOfBoundsException exception) {
				exception.printStackTrace();
				s = "Empty field";
			}
			return s;
		}
		return null;
	}
}
