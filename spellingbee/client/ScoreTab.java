package spellingbee.client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import spellingbee.network.Client;

import java.awt.*;

/**
 * ScoreTab class, this class creates a custom tab
 * with the user interface required to show the score
 * of the spelling bee game.
 * @param Client client passed from the spellingBeeClient.
 * @param CScore current score.
 * @param col2brackets stores all the possible brackets for the set of letters.
 * @param col1Categories stores the categories for each bracket.
 * */
public class ScoreTab extends Tab {
    private Client client;
    private Label CScore = new Label("0");
    private Text[] col2brackets = new Text[5];
    private Text[] col1Categories = new Text[5];

    /**
     * The constructor contains all the elements and nodes, for the UI.
     * It also request information to the server to display the letters,
     * get the score for each word entered. It uses a helper method
     * to set a GridPane
     */
    public ScoreTab(Client client){
        super("Score");
        this.client = client;
        GridPane gridPane = setGridPane();
        this.setContent(gridPane);
    }
    /*The Refresh method updates the score made by the player
     * it requests the score to the server and prints it 
     * every time a word is guessed */
    public void refresh(){
        int score = Integer.parseInt(client.sendAndWaitMessage("getScore"));
        this.CScore.setText(String.valueOf(score));
        for (int i = 0; i < col2brackets.length; i++) {
            if (Integer.parseInt(col2brackets[i].getText()) <= score) {
                col1Categories[i].setFill(Color.GREEN);
                col2brackets[i].setFill(Color.GREEN);
            }
        }

    }
    /* the gridPane contains all the User interface elements
     * to show all the information about the game, as it is 
     * the score and the maximum score the player can get.*/
    private GridPane setGridPane(){
        col1Categories[0] = new Text("Queen Bee");
        col1Categories[1] = new Text("Genius");
        col1Categories[2] = new Text("Amazing");
        col1Categories[3] = new Text("Not Bad");
        col1Categories[4] = new Text("Starter");
        
        Text currentScore = new Text("Current Score");

        String[] message = this.client.sendAndWaitMessage("getBrackets").split(":",-1);

        col2brackets[0] = new Text(message[4]);
        col2brackets[1] = new Text(message[3]);
        col2brackets[2] = new Text(message[2]);
        col2brackets[3] = new Text(message[1]);
        col2brackets[4] = new Text(message[0]);
  
        //Creating a Grid Pane
        GridPane gridPane = new GridPane();

        //Setting size for the pane
        gridPane.setMinSize(400, 200);

        //Setting the padding
        gridPane.setPadding(new Insets(10, 10, 10, 10));

        //Setting the vertical and horizontal gaps between the columns
        gridPane.setVgap(5);
        gridPane.setHgap(5);

        //Setting the Grid alignment
        gridPane.setAlignment(Pos.CENTER_LEFT);

        //Adding the elements to two different columns
        gridPane.addColumn(0, col1Categories[0],col1Categories[1],col1Categories[2],col1Categories[3],col1Categories[4]);
        gridPane.addColumn(1, col2brackets[0],col2brackets[1],col2brackets[2],col2brackets[3],col2brackets[4]);
        gridPane.addRow(5, currentScore, CScore);

        /*using an anonimus function to set the event on the
        button*/
        Button btn = new Button("Refresh");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                refresh();
            }
        });
        /*finally everything is added to a Gridpane and it
         * is sent to the constructor.*/
        gridPane.add(btn, 0,6);

        return gridPane;
    }

}
