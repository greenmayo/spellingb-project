package spellingbee.client;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import spellingbee.network.Client;

/**
 * GameTab class, this class creates a custom tab
 * with the user interface required to play the spelling
 * bee game.
 * @param Client client passed from the spellingBeeClient
 * @param UiHelper Helper class to add eventListeners
 * @param l2 Label for the each word points.
 */

public class GameTab extends Tab{
	private Client client;
	private UiHelper ui;
	Label l2 = new Label("0");
/**
 * The constructor contains all the elements and nodes, for the UI.
 * It also request information to the server to display the letters,
 * get the score for each word entered.
 */
	public GameTab(Client client) {
		super("Game");
		this.client = client;
		
		/*TextField created and styled.*/
		TextField txt = new TextField();
		txt.setMaxWidth(450);
		txt.setStyle("-fx-padding: 5px");
		
		//Requesting to the server the 7 letters
		String[] letters = this.client.sendAndWaitMessage("getAllLetters").split("");
		String centerLetter = this.client.sendAndWaitMessage("getCenterLetter");

		/*Creating an array of buttons and adding the letters gotten from the server,
		 * it also adds the event listener to click the button and add the letter
		 * to the text field*/
		int index=0;
		Button[] bLetters = new Button[6];
		Button bCenter = null;

		for (String letter : letters) {
			if (letter.equals(centerLetter)) {
				bCenter = new Button(letter);
				
				ui = new UiHelper(txt,letter,"letters");

				bCenter.setOnAction(ui);
			} else {
				bLetters[index] = new Button(letter);
				ui = new UiHelper(txt,letter,"letters");
				bLetters[index].setOnAction(ui);
				index++;
			}
		}
		
		//Styling the centered letter
		bCenter.setStyle("-fx-text-fill: red");
		
		/*Creating the submit, delete and clear buttons*/
		Button sub = new Button("Submit");
		Button del = new Button("Del");
		Button clear = new Button("clear");
		
		Label l1 = new Label("Welcome!");
		l1.setStyle("-fx-padding: 5 10;-fx-border-color: grey");
		l1.setPrefWidth(250);
		l2.setStyle("-fx-padding: 5 10;-fx-border-color: grey");
		l2.setPrefWidth(150);
		/*Adding the events to the submit button by creating a new
		 * object with a modified constructor, it does the same with
		 * the delete and clear buttons*/
		ui = new UiHelper(txt,l1,l2,"submit",client);
		sub.setOnAction(ui);
		ui = new UiHelper(txt,"del",client);
		del.setOnAction(ui);
		ui = new UiHelper(txt,"clear",client);
		clear.setOnAction(ui);
		
		/*Nodes created to wrap all the elements into the tab*/
		VBox v1 = new VBox();
		HBox h1 = new HBox();
		HBox h2 = new HBox();
		HBox buttons = new HBox();

		/*Adding all the elements to the nodes and also styling so it looks centered*/
		h1.getChildren().addAll(bLetters[0],bLetters[1],bLetters[2],bCenter,bLetters[3],bLetters[4],bLetters[5]);
		h1.setSpacing(10);
		h1.setStyle("-fx-padding: 0 50 10");
		h2.getChildren().addAll(l1,l2);
		h2.setSpacing(30);
		h2.setStyle("-fx-padding: 10");
		buttons.getChildren().addAll(sub,del,clear);
		buttons.setSpacing(20);
		buttons.setStyle("-fx-padding: 10 70 0");
		
		v1.getChildren().addAll(h1,txt,buttons,h2);
		v1.setStyle("-fx-padding: 20");
		
		/*Setting the first node that contains all the other nodes to the Tab*/
		this.setContent(v1);
	}
	
	/*Getter created to return the label that contains
	 * the points for each word*/
	public Label getScoreField(){
		return this.l2;
	}

}
