package spellingbee.client;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.text.Text;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.*;
import spellingbee.network.Client;

/**
 * SpellingBeeClient class, this class is our connection
 * with the client and therefore the server.
 * of the spelling bee game.
 * @param Client client .
 * */
public class SpellingBeeClient extends Application {
    private Client client = new Client();

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) {
    	/*tabPane created to add the customs tabs (Score, Game)
    	 * and the close option is disabled.*/
        TabPane tabPane = new TabPane();
        tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        //Creating a Game tab
        GameTab game = new GameTab(client);
        //Creating a score Tab
        ScoreTab score = new ScoreTab(client);
        //Adding the two tabs to the tabPane
        tabPane.getTabs().addAll(game,score);
        /*Wrapping, styling and setting the size parameters
         * for the scene*/
        VBox vBox = new VBox(tabPane);
        Scene scene = new Scene(vBox, 500, 300);
        scene.getStylesheets().add("styling/styles.css");

        // score event change listener
        Label l2 = game.getScoreField();
        l2.textProperty().addListener((observableValue, s, t1) -> {
            score.refresh();
        });
        
        primaryStage.setScene(scene);
        primaryStage.setTitle("JavaFX App");

        primaryStage.show();

    }
}
