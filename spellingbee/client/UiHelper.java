package spellingbee.client;

import javafx.event.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import spellingbee.network.Client;

public class UiHelper implements EventHandler<ActionEvent> {
	private TextField txt;
	private String letter;
	private String handler;
	private Label message;
	private Label points;
	private Client client;
	
	public UiHelper(TextField txt,String letter,String handler){
		this.txt = txt;
		this.letter = letter;
		this.handler = handler;
	}
	
	public UiHelper(TextField txt,Label message,Label points, String submit,Client client) {
		this.txt = txt;
		this.message = message;
		this.points = points;
		this.handler = submit;
		this.client = client;
	}
	
	public UiHelper(TextField txt,String handler, Client client) {
		this.txt = txt;
		this.handler = handler;
		this.client = client;
	}
	

	@Override
	public void handle(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(handler.equals("letters")) {
			txt.setText(txt.getText() + letter);
		}
		else if(handler.equals("submit")) {
			try {
			String[] text = client.sendAndWaitMessage("play:"+txt.getText()).split(":");
			if(text[0] != null) {
				message.setText(text[0]);
				points.setText(text[1]);
			}
			}catch(ArrayIndexOutOfBoundsException exception) {
				exception.printStackTrace();
				
			}
			
		}
		else if(handler.equals("del")) {
			String textField = txt.getText();
			if(textField.length()!=0) {
				txt.setText(textField.substring(0, textField.length()-1));
			}
		}
		else if(handler.equals("clear")) {
			txt.setText("");
		}
	}

}
