Project 2: Spelling Bee!

Project for Java III class that made us get to know with JavaFX and omproved our knowledge of classes, objects and connections between packages


How to Play
Create words using letters from the top.

    Words must contain at least 4 letters.
    Words must include the center letter.
    Our word list does not include words that are obscure, hyphenated, or proper nouns.
    Letters can be used more than once.

Score points to increase your rating.

    4-letter words are worth 1 point each.
    Longer words earn 1 point per letter.
    Each puzzle includes at least one “pangram” which uses every letter. These are worth 7 extra points!
